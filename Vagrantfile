# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "bento/ubuntu-16.04"
  #config.vm.box = "boxcutter/ubuntu1604-desktop"

  config.vm.provider "virtualbox" do |vb|
    vb.name = "ubuntu-dev-server"
    vb.memory = "4096"
  end

  # Microsoft SQL server
  config.vm.network "forwarded_port", guest: 1433, host: 1433
  # mysql
  config.vm.network "forwarded_port", guest: 3306, host: 3306
  # IBM DB2
  config.vm.network "forwarded_port", guest: 50000, host: 50000
  # IBM MQ
  config.vm.network "forwarded_port", guest: 1414, host: 1414
  # Angular CLI
  config.vm.network "forwarded_port", guest: 4200, host: 4200

  # Web and app servers
  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 8443, host: 8443
  config.vm.network "forwarded_port", guest: 8888, host: 8888
  config.vm.network "forwarded_port", guest: 9043, host: 9043
  config.vm.network "forwarded_port", guest: 9080, host: 9080
  config.vm.network "forwarded_port", guest: 9443, host: 9443

  config.vm.synced_folder "~/workspace", "/home/vagrant/workspace"
  config.vm.synced_folder "~/.m2", "/home/vagrant/.m2"

  config.vm.provision "docker"
  config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
  config.vm.provision "shell", inline: "echo 'vagrant  ALL= (ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers"
  config.vm.provision "shell", inline: "apt-get update -y"
  config.vm.provision "shell", path: "provision/docker-compose.sh"
  config.vm.provision "shell", path: "provision/openjdk-8.sh"
  config.vm.provision "shell", path: "provision/maven.sh"
  config.vm.provision "shell", path: "provision/node.sh"

  config.vm.provision "shell", inline: "echo 'ubuntu-dev-server ready for development'"
end
