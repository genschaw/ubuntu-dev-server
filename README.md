# README #

**Required Software**

- [git](https://git-scm.com/)
- [Virtual Box](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)

### What is this repository for? ###

This repository defines a Vagrant box which provisions an Ubuntu VirtualBox VM
Development Server.

This server supports the development environment, allows developers running older windows machines to run docker, and provides a repeatable image for research and development purposes.

It has the following applications installed:
* Git
* Java JDK 8
* Maven
* Node & npm
* Docker & Docker Compose

### How do I get set up? ###

* Clone this repository and use Vagrant to provision the machine
* Configuration - Vagrantfile
    * Exposes a number of ports to support database servers, messaging servers, web servers and application servers.
    * Mounts your local ~/.gitconfig file and ~/.m2 directory from the host. This allows for using the git cli, and sharing your local maven repository within the machine.
    * Mounts a local workspace directory from the host. This allows for using your local IDE on the host for source code editing, and using the linux environment for application build and execution.

From a command shell, run the following:
```bash
cd ~/workspace
git clone https://bitbucket.org/genschaw/ubuntu-dev-server.git
cd ubuntu-dev-server
vagrant up
vagrant ssh
# run some sample commands to verify setup
git --version
java -version
mvn -v
node -v
npm -v
docker -v
docker-compose -v
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)