#!/usr/bin/env bash
echo 'Installing Docker Compose onto machine...'
curl -fsSL https://github.com/docker/compose/releases/download/1.11.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo 'docker -v:' && docker -v
echo 'docker-compose -v:' && docker-compose -v
