#!/usr/bin/env bash
echo 'Installing Maven onto machine...'
MAVEN_VERSION=3.3.9
MAVEN_INSTALLDIR=/opt

mkdir -p $MAVEN_INSTALLDIR
curl -fksSL http://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \
  | tar -xzC $MAVEN_INSTALLDIR
ln -fs $MAVEN_INSTALLDIR/apache-maven-$MAVEN_VERSION/bin/mvn /usr/local/bin/mvn

echo 'mvn -v:' && mvn -v
