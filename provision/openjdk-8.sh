#!/usr/bin/env bash
echo 'Installing OpenJDK 8 onto machine...'
apt-get install -y openjdk-8-jdk
echo 'JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64' >> /etc/environment
echo 'java -version:' && java -version
echo 'javac -version:' && javac -version