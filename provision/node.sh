#!/usr/bin/env bash
echo 'Installing Node onto machine...'

NODE_VERSION=6.9.5
INSTALLDIR=/opt

mkdir -p $INSTALLDIR

curl -fksSL https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz \
  | tar -xzC $INSTALLDIR

ln -fs $INSTALLDIR/node-v$NODE_VERSION-linux-x64/bin/node /usr/local/bin/node
ln -fs $INSTALLDIR/node-v$NODE_VERSION-linux-x64/bin/npm /usr/local/bin/npm

echo 'node -v:' && node -v
echo 'npm -v:' && npm -v
